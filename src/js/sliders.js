var bonuses = new Swiper('.js-bonuses-slider', {
    slidesPerView: 1,
    spaceBetween: 0,
    pagination: {
        clickable: true,
        el: '.js-bonuses-pagination',
    },
    breakpoints: {
        480: {
            slidesPerView: 2,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        992: {
            slidesPerView: 4,
            spaceBetween: 15,
        },
        1200: {
            slidesPerView: 4,
            spaceBetween: 20,
        },
        1530: {
            slidesPerView: 4,
            spaceBetween: 30,
        }
    }
});

var restaurants = new Swiper('.js--init-restaurants', {
    slidesPerView: 1,
    spaceBetween: 0,
    effect: 'fade',
    speed: 400,
    pagination: {
        clickable: true,
        el: '.js--pagination-restaurants',
    },
});

var stories = new Swiper('.js--init-stories', {
    slidesPerView: 1,
    spaceBetween: 15,
    speed: 400,
    watchOverflow: true,
    pagination: {
        clickable: true,
        el: '.js--pagination-stories',
    },
    breakpoints: {
        481: {
            slidesPerView: 2,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        1530: {
            slidesPerView: 4,
            spaceBetween: 15,
        }
    }
});

var menu = new Swiper('.js--init-menu', {
    slidesPerView: 1,
    spaceBetween: 20,
    speed: 500,
    watchOverflow: true,
    pagination: {
        clickable: true,
        el: '.js--pagination-menu',
    },
    breakpoints: {
        992: {
            slidesPerView: 2,
            spaceBetween: 20,
        }
    }
});

