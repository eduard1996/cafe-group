var animateWindowScroll,
    animateBlock,
    headerHeightAnimate;

headerHeightAnimate = $('.header').height();

animateBlock = $('.guest').offset().top - headerHeightAnimate;

$(document).on('scroll', function() {

    animateWindowScroll = $(window).scrollTop();

    if(animateWindowScroll >= animateBlock) {
        $('.guest').addClass('scroll-guest');
    }
})