$(document).ready(function() {

    //=include modals.js
    //=include sliders.js
    //=include other.js
    //=include scroll.js
    //=include animate.js
    //=include works-tab.js
    //=include tabs.js
    //=include form.js
    //=include parallax.js

    var scrollbarCustom;

    $('.js-init-scrollbar').each(function( index ) {
        
        scrollbarCustom = new SimpleBar(this);
    });

})