$(document).on('click', '[data-rtab]', function(e) {

    e.preventDefault();
    
    var idData, elShow;

    $('[data-rtab]').removeClass('active');

    $(this).addClass('active');

    idData = $(this).data('rtab');

    elShow = $('[data-rcollapse=' + idData + ']');

    if(idData != 'all') {

        $('[data-rcollapse]').removeClass('ractive').addClass('rdisabled');
    
        elShow.removeClass('rdisabled').addClass('ractive');

    } else {

        $('[data-rcollapse]').removeClass('rdisabled').addClass('ractive');
    }
})