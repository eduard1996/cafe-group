$('.js--init-inputmask').inputmask({
    mask: "+7 ( 999 ) 999-99-99",
    showMaskOnHover: false,
});

$(document).on('click', '.modal', function(e) {
    
    var div = $(".modal__container"); 
    
    if (!div.is(e.target) && div.has(e.target).length === 0) { 
        $('.modal').fadeOut(300);
    }
})

$(document).on('click', '.modal__close', function(e) {
    
    $('.modal').fadeOut(300);
})

$('.js--init-gender').select2({
    minimumResultsForSearch: -1,
    width: '100%',
})

$('.js--init-city').select2({
    width: '100%',
    language: {
        noResults: function(term) {
            return "Нет результатов";
       }
   }
})