$(document).on('click', '.js-toggle-navbar', function(){
    $(this).find('.toggle__icon').toggleClass('active');
    $('.js-show-navbar').toggleClass('active');
})

$(document).mouseup(function (e) {
    var $elemet = $(".js-show-navbar, .js-toggle-navbar");
    if (!$elemet.is(e.target) && $elemet.has(e.target).length === 0) {
        $('.js-toggle-navbar').find('.toggle__icon').removeClass('active');
        $('.js-show-navbar').removeClass('active');
    }
});

$('.js-tilt').tilt();