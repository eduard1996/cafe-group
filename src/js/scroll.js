$(".js-scroll-link").on('click', function () {

    var idLink, toScroll, headerHeight, $this;

    $this = $(this);

    headerHeight = $('.header').height();

    idLink = $this.attr('href').substr(1);

    toScroll = $('[data-scroll=' + idLink + ']');

    $('html, body').animate({ scrollTop: toScroll.offset().top - headerHeight }, 1000);
});