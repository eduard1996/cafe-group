$('[data-tab]').on('click', function() {

    var workTab,
        workCollapse;

    if($(window).width() > 767) {

        workTab = $(this).data('tab');
        workCollapse = $('[data-collapse="' + workTab + '"]');
    
        $('[data-tab]').removeClass('active');
        $('[data-collapse]').addClass('hide').removeClass('active');
    
        $(this).addClass('active');
        workCollapse.removeClass('hide').addClass('active');
    }
})