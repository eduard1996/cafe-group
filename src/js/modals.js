$(document).on('click', '.js--open-bonuses', function(e) {

    e.preventDefault();

    $('.modal--bonuses').fadeIn(300);
})

$(document).on('click', '.js--open-registration', function(e) {

    e.preventDefault();

    $('.modal--registration').fadeIn(300);
})

$(document).on('click', '.js--open-data', function(e) {

    e.preventDefault();

    $('.modal--data').fadeIn(300);
})

$(document).on('click', '.js--open-resume', function(e) {

    e.preventDefault();

    $('.modal--resume').fadeIn(300);
})

$(document).on('click', '.js--open-reservation', function(e) {

    e.preventDefault();

    $('.modal--reservation').fadeIn(300);
})

