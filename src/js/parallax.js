var statusScroll = 0;

var aboutScroll = 0,
    restaurantScroll = 0;

$(document).on('scroll', function() {

    //_________ about
    parallaxUP(
        $('.about-art__line-left'), 
        $('.about-art__line-right'), 
        $('.about'), 
        statusScroll);
    parallaxDown(
        $('.about-art__line-left'), 
        $('.about-art__line-right'), 
        $('.about'), 
        statusScroll);
    
    //_________ restaurant
    parallaxUP(
        $('.restaurant-art__line-left'), 
        $('.restaurant-art__line-right'), 
        $('.restaurant'), 
        statusScroll);
    parallaxDown(
        $('.restaurant-art__line-left'), 
        $('.restaurant-art__line-right'), 
        $('.restaurant'), 
        statusScroll);

    //_________ guest
    parallaxUP(
        $('.guest-art__line-left'), 
        $('.guest-art__line-right'), 
        $('.guest'), 
        statusScroll);
    parallaxDown(
        $('.guest-art__line-left'), 
        $('.guest-art__line-right'), 
        $('.guest'), 
        statusScroll);

    //_________ bonuses
    parallaxUP(
        $('.bonuses-art__line-left'), 
        $('.bonuses-art__line-right'), 
        $('.bonuses'), 
        statusScroll);
    parallaxDown(
        $('.bonuses-art__line-left'), 
        $('.bonuses-art__line-right'), 
        $('.bonuses'), 
        statusScroll);

    //_________ works
    parallaxUP(
        $('.works-art__line-left'), 
        $('.works-art__line-right'), 
        $('.works-art'), 
        statusScroll);
    parallaxDown(
        $('.works-art__line-left'), 
        $('.works-art__line-right'), 
        $('.works-art'), 
        statusScroll);

    //_________ news
    parallaxUP(
        $('.news-art__line-left'), 
        $('.news-art__line-right'), 
        $('.news'), 
        statusScroll);
    parallaxDown(
        $('.news-art__line-left'), 
        $('.news-art__line-right'), 
        $('.news'), 
        statusScroll);

    statusScroll = $(window).scrollTop();
})

function parallaxUP($left, $right, $parent, $s) {

    if($(window).scrollTop() > $s) {

        $left.css('background-position-y', $s / 4 + 'px');
        $right.css('background-position-y', $s / 5 + 'px');
    }
}

function parallaxDown($left, $right, $parent, $s) {

    if($(window).scrollTop() < $s) {

        $left.css('background-position-y', $s / 4 + 'px');
        $right.css('background-position-y', $s / 5 + 'px');
    }
}